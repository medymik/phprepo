<?php
     $pdo = new PDO("mysql:host=localhost;dbname=phpcours", 'root', 'root');

     $sql_query = 'select * from message';

     $query = $pdo->prepare($sql_query);
     
     $query->execute();

     $messages = $query->fetchAll(PDO::FETCH_ASSOC);


?>
<?php include 'shared/header.php'; ?>
<?php include 'auth_filter.php';?>
    
    <div class="container">
    <h3>Listes des messages</h3>
    <a href="contact.php">New</a>
    <table class="table">
    <thead>
        <tr>
        <th>Nom</th>
        <th>Email</th>
        <th>Message</th>
        <th>Actions</th>
        </tr>
    </thead>
    <?php foreach($messages as $message){ ?>
        <tr>
            <td><?php echo $message['nom']; ?></td>
            <td><?php echo $message['email']; ?></td>
            <td><?php echo $message['message']; ?></td>

            <td><a href="<?php echo 'supprimer_message.php?id='.$message['id']; ?>">Delete</a></td>
            <td><a href="<?php echo 'edit.php?id='.$message['id']; ?>">Edit</a></td>
        </tr>
    <?php } ?>
    </table>
    
        
    </div>

<?php include 'shared/footer.php'; ?>