<?php
    if(isset($_POST['contactez']))
    {
        if(!empty($_POST['nom']) && !empty($_POST['email']) && !empty($_POST['message']))
        {
            // 
            $pdo = new PDO("mysql:host=localhost;dbname=phpcours", 'root', 'root');

            $sql_query = 'insert into message(nom,email,message) values (?,?,?)';

            $query = $pdo->prepare($sql_query);

            $query->execute([$_POST['nom'],$_POST['email'],$_POST['message']]);

            // redirection
            header('location:messages.php');

            

        }
    }
?>
<?php include 'shared/header.php'; ?>

<?php include 'auth_filter.php';?>
    
    <div class="container">
    <h3>Contactez nous</h3>
        <form method="post">
            <div class="form-group">
                <label for="nom">Nom</label>
                <input type="text" name="nom" class="form-control">
            </div>

            <div class="form-group">
                <label for="nom">Email</label>
                <input type="text" name="email" class="form-control">
            </div>

            <div class="form-group">
                <label for="nom">Message</label>
                <textarea name="message" class="form-control"></textarea>
            </div>

            <button name="contactez" class="btn btn-primary">Valider</button>

        </form>
    </div>

<?php include 'shared/footer.php'; ?>