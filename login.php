<?php 
include 'shared/header.php';
include 'guest_filter.php';


$errors = [];

if(isset($_POST['connect'])){
    if(!empty($_POST['email']) && !empty($_POST['password'])){
            // validation d'email
            if(filter_var($_POST['email'],FILTER_VALIDATE_EMAIL)){
                

                $pdo = new PDO("mysql:host=localhost;dbname=phpcours", 'root', 'root');

                $sql_query = 'select * from users where email = ? and password = ?';

                $query = $pdo->prepare($sql_query);
     
                $query->execute([$_POST['email'],$_POST['password']]);

                $user = $query->fetch(PDO::FETCH_ASSOC);

                if($user){

                    $_SESSION['user'] = $user;
                    
                    header('location:messages.php');

                }else{
                    $errors[] = " Authentification incorrect !";
                }



            }else {
                $errors[] = "Votre email invalide !";
            }
    }else{
        $errors[] = "Tous les champs sont obligatoires !";
    }
}

?>



<div class="container">
        
    <h3>Connexion</h3>

    <?php if(count($errors)>0){ ?>
            <div class="alert alert-danger">
                <?php foreach($errors as $error){ ?>
                    <p> <?php echo $error; ?></p>
                <?php } ?>
            </div>
    <?php } ?>

        <form method="post">
            <div class="form-group">
                <label for="email">email</label>
                <input type="text" name="email" class="form-control">
            </div>

            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" name="password" class="form-control">
            </div>


            <button name="connect" class="btn btn-primary">Se connecter</button>

        </form>
    </div>

<?php include 'shared/footer.php'; ?>